use oo7::portal::SecretProxy;
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::UnixStream,
};
use tracing::{debug, error, info};
use zbus::Connection;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let connection = Connection::session()
        .await
        .expect("Couldn't open session bus");

    let proxy = SecretProxy::new(&connection)
        .await
        .expect("Couldn't open secret proxy");

    let mut last_sum = 0;
    // Try three times, to be sure
    for attempt in 1..4 {
        info!("Attempt #{attempt}");

        // Use a unix socket to send the secret over
        let (mut reader, mut writer) = UnixStream::pair().expect("Couldn't create fd pair");
        // SecretProxy::retrieve_secret handles the
        // org.freedesktop.portal.Secrets `RetrieveSecret` call for us
        proxy
            .retrieve_secret(&writer)
            .await
            .expect("Couldn't get secret");
        writer.shutdown().await.ok();

        let mut buf = Vec::new();
        reader
            .read_to_end(&mut buf)
            .await
            .expect("Couldn't read secret fd");

        // Get a CRC32 to make the secret data a bit easier to compare
        let crc = crc::Crc::<u32>::new(&crc::CRC_32_CKSUM);
        let sum = crc.checksum(&buf);

        info!("Secret CRC: {:x}", sum);
        debug!("Secret data: {:x?}", &buf);

        // We should receive the same secret every time we call KWallet.
        // Otherwise, we don't have the password to unlock our own secrets
        if last_sum != 0 && sum != last_sum {
            error!(
                "Secrets don't match (old CRC: {:x?}, new CRC: {:x?})",
                last_sum, sum
            );
        }

        last_sum = sum;
    }
}

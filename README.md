# XDG Secret Portal test program

This program performs a basic test of the `org.freedesktop.portal.Secret` interface, using [oo7] to call `RetrieveSecret` and retrieve the result. This is run three times, in order to verify the application secret is consistent.

## Running as a Flatpak

Assuming you have Flathub as a user repository, the following should work:

```sh
flatpak-builder --force-clean \
    --install   \
    --user      \
    outdir xyz.datagirl.SecretPortalRepro.yml
flatpak run xyz.datagirl.SecretPortalRepro
```

You might also want to run the Flatpak with debug logging:

```sh
flatpak run --env=RUST_LOG=debug xyz.datagirl.SecretPortalRepro
```

[oo7]: https://crates.io/crates/oo7